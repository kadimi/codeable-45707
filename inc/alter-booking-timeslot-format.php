<?php

/**
 * Modify time slot display from `09:00` to `09:00 - 10:00`. 
 */
add_action( 'wp_footer', function() {


	/**
	 * Add JavaScript
	 */
	?>
	<script>

		function zeroFill( number, width ) {
			width -= number.toString().length;
			if ( width > 0 ) {
				return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
			}
			return number + ""; // always return a string
		}

		jQuery( document ).ready( function( $ ) {

			var $bookingTimes = $( '.variation-BookingTime p, .variation-BoekingTijd p' );
			$bookingTimes.each( function() {

				var $this     = $( this );
				var hourStart = parseInt( $this.text().slice( 0, 2 ) );
				var hourEnd   = ( hourStart + 1 ) % 24;
				var newText   = '{1}:00 - {2}:00'
					.replace( '{1}', zeroFill( hourStart , 2 ) )
					.replace( '{2}', zeroFill( hourEnd, 2 ) )
				;

				/**
				 * Update time slots.
				 */
				$this.text( newText );
			} );

			setInterval( function() {

				var $             = jQuery;
				var $blocks       = $( 'li.block' );

				$blocks.each( function() {

					var $this     = $( this );
					var hourStart = parseInt( $this.attr( 'data-block' ).slice( 0, 2 ) );
					var hourEnd   = ( hourStart + 1 ) % 24;
					var $a        = $( 'a', $this );
					var newText   = '{1}:00 - {2}:00'
						.replace( '{1}', zeroFill( hourStart , 2 ) )
						.replace( '{2}', zeroFill( hourEnd, 2 ) )
					;

					/**
					 * Update time slots exiting loop if first value is alreay updated.
					 */
					if ( $a.text() !== newText ) {
						$a.text( newText );
					} else {
						return false;
					}
				} );
			}, 500 );
		} );
	</script>
	<?php
} );
