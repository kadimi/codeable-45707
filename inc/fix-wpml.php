<?php

add_action( 'init', function() {

	/**
	 * Faulty translations elements ids.
	 * @var Array
	 */
	$faulty_element_ids = [
		11111
	];

	/**
	 * Run every 24 hours.
	 */
	$done = get_transient( 'codeable_45707_fix_wpml' );
	if ( $done ) {
		return;
	}
	set_transient( 'codeable_45707_fix_wpml', time(), DAY_IN_SECONDS );


	/**
	 * Remove faulty translations.
	 */
	global $wpdb;
	foreach ( $faulty_element_ids as $faulty_element_id ) {
		$wpdb->update( $wpdb->prefix . 'icl_translations'
			, [ 'element_id' => $faulty_element_id + 1000000 ]
			, [ 'element_id' => $faulty_element_id ]
			, [ '%d' ]
			, [ '%d' ]
		);
	}
} );
