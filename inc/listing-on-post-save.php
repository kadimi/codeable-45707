<?php

// Disable i18n.
// add_action( 'gettext', function( $a, $b, $c ) { return $b; }, 10, 3 );

/**
 * Save terms that are skipped by form craft.
 */
add_action( 'save_post', function( $post_id ) {

	$post = get_post( $post_id );

	/**
	 * Run only when saving a jpb listing through Formcraft.
	 */
	if ( 0
		|| 'job_listing' !== $post->post_type                                               // Not a job listing.
		|| empty( $_REQUEST['action'] ) || 'formcraft3_form_submit' !== $_REQUEST['action'] // Not saving with Formcraft
	) {
		return;
	}

	/**
	 * Key-taxonomy combinations as in Formcraft forms.
	 * @var Array
	 */
	$key_taxonomy = [
		'field1' => 'job_listing_type',
		'field5' => 'job_listing_category',
	];

	/**
	 * Step through keys and set terms/taxonomies.
	 *
	 * - Only when key is in request.
	 * - Add terms to post, making sure we pass term ids as integers (see function docs)
	 */
	foreach ( $key_taxonomy as $key => $taxonomy ) {
		if ( ! empty( $_REQUEST[ $key ] ) ) {
			wp_set_object_terms( $post_id, array_map( 'intval', $_REQUEST[ $key ] ), $taxonomy );
		}

	}
} );


/**
 * Creates product and vendor for listing once the listing is saved.
 */
add_action( 'save_post', function( $post_id ) {

	$user =	wp_get_current_user();

	$post = get_post( $post_id );

	/**
	 * Only `job_listing`.
	 */
	if ( 'job_listing' !== $post->post_type ) {
		return;
	}

	$author = get_userdata( $post->post_author );
	$new_product_title   = sprintf( 'Wasservice van %s', $author->nickname );
	$new_product_content = sprintf( 'Dit is de wasservice van %s', $author->nickname );
	$new_product_meta    = [
		'_wc_booking_has_persons'               => 'yes',
		'_wc_booking_min_duration'              => '1',
		'_wc_booking_max_duration'              => '1',
		'_wc_booking_calendar_display_mode'     => 'always_visible',
		'_wc_booking_qty'                       => '100',
		'_wc_booking_person_qty_multiplier'     => 'no',
		'_wc_booking_person_cost_multiplier'    => 'no',
		'_wc_booking_min_persons_group'         => '1',
		'_wc_booking_max_persons_group'         => '70',
		'_wc_booking_has_person_types'          => 'yes',
		// '_wc_booking_has_resources'             => 'yes',
		// '_wc_booking_resouce_label'             => 'Bezorging',
		// '_wc_booking_resources_assignment'      => 'customer',
		'_wc_booking_duration_type'             => 'fixed',
		'_wc_booking_duration'                  => '1',
		'_wc_booking_cost'                      => '0',
		'_price'                                => '10',
		'_wc_booking_base_cost'                 => '0',
		'_wc_booking_duration_unit'             => 'hour',
		'_wc_booking_user_can_cancel'           => 'yes',
		'_wc_booking_cancel_limit'              => '1',
		'_wc_booking_cancel_limit_unit'         => 'month',
		'_wc_booking_max_date'                  => '12',
		'_wc_booking_max_date_unit'             => 'month',
		'_wc_booking_min_date_unit'             => 'month',
		'_wc_booking_first_block_time'          => '09:00',
		'_wc_booking_requires_confirmation'     => 'no',
		'_wc_booking_default_date_availability' => 'available',
		'_has_additional_costs'                 => 'yes',
		'_product_image_gallery'                => '11155',
		'_thumbnail_id'                         => '11155',
	];
	$new_vendor_name = sprintf( '%s (%s)', $author->user_login, $author->ID );

	/**
	 * Only for employers.
	 */
	$user_is_employer = user_can(  $post->post_author, 'employer' );
	if( ! $user_is_employer ) {
		return;
	}

	/**
	 * Create product or use existing.
	 */
	$product_exists = get_page_by_title( $new_product_title, null, 'product' );
	if ( $product_exists ) {
		$new_product_id = $product_exists->ID;
	} else {
		$new_product_id = wp_insert_post( array(
			'post_author'  => $post->post_author,
			'post_type'    => 'product',
			'post_title'   => $new_product_title,
			'post_content' => $new_product_content,
			'post_status'  => 'publish',
		) );
		/**
		 * Make the product a booking.
		 */
		wp_set_object_terms( $new_product_id, 'booking', 'product_type' );

		/**
		 * Add initial meta data.
		 */
		foreach ( $new_product_meta as $meta_key => $meta_value ) {
			add_post_meta( $new_product_id, $meta_key, $meta_value );
		}

		/**
		 * Add person type 1.
		 */
		$person_type_id = wp_insert_post( [
			'post_title'   => 'Wassen & Vouwen',
			'post_excerpt' => '1 trommel (5kg)',
			'post_status'  => 'publish',
			'post_author'  => $post->post_author,
			'post_parent'  => $new_product_id,
			'post_type'    => 'bookable_person',
			'menu_order'   => 1,
		] );
		update_post_meta( $person_type_id, 'cost',       wc_clean( 0 ) );
		update_post_meta( $person_type_id, 'block_cost', wc_clean( 10 ) );
		update_post_meta( $person_type_id, 'min',        wc_clean( 0 ) );
		update_post_meta( $person_type_id, 'max',        wc_clean( 10 ) );

		/**
		 * Add person type 2.
		 */
		$person_type_id = wp_insert_post( [
			'post_title'   => 'Wassen & Strijken',
			'post_excerpt' => '1 trommel (5kg)',
			'post_status'  => 'publish',
			'post_author'  => $post->post_author,
			'post_parent'  => $new_product_id,
			'post_type'    => 'bookable_person',
			'menu_order'   => 1,
		] );
		update_post_meta( $person_type_id, 'cost',       wc_clean( 0 ) );
		update_post_meta( $person_type_id, 'block_cost', wc_clean( 15 ) );
		update_post_meta( $person_type_id, 'min',        wc_clean( 0 ) );
		update_post_meta( $person_type_id, 'max',        wc_clean( 10 ) );
	}

	/**
	 * Add product to listing.
	 *
	 */
	update_post_meta( $post_id, '_products', [ ( string ) $new_product_id ] );

	/**
	 * Create vendor.
	 */
	$term_exists = get_term_by('name', $new_vendor_name, WC_PRODUCT_VENDORS_TAXONOMY, ARRAY_A );
	if ( $term_exists ) {
		$new_vendor_term = $term_exists;
	} else {
		$new_vendor_term = wp_insert_term( $new_vendor_name, WC_PRODUCT_VENDORS_TAXONOMY );
	}

	/**
	 * Set vendor data.
	 */
	update_term_meta( $new_vendor_term[ 'term_id' ], 'vendor_data', [
		'email'   => $author->user_email,
		'profile' => $author->user_email,
	] );

	/**
	 * Set vendor term for product.
	 */
	wp_set_object_terms( $new_product_id, $new_vendor_term[ 'term_id' ], WC_PRODUCT_VENDORS_TAXONOMY );
} );
