<?php

/**
 * Add name and phone number
 */
add_action( 'woocommerce_email_order_details', function( $order, $sent_to_admin, $plain_text, $email ) {

	/**
	 * Only on order processing emails.
	 */
	if( ! is_a( $email, 'WC_Email_Customer_Processing_Order' ) ) {
		return $order;
	}

	$vendor_slug    = current( WC_Product_Vendors_Utils::get_vendors_from_order( $order ) )['slug'];
	$vendor_user_id = ( int ) end( explode( '-', $vendor_slug ) );
	$vendor_user    = get_user_by( 'id', $vendor_user_id );

	?>
	<h2><?php _e( 'Service information', 'codeable-45707' ) ;?></h2>
	<ul>
		<li>
			<strong><?php _e( 'Name', 'woocommerce' ); ?>:</strong>
			<span class="text"><?php echo wp_kses_post( get_user_meta( $vendor_user_id, 'nickname', true ) ); ?></span>
		</li>
		<li>
			<strong><?php _e( 'Phone', 'woocommerce' ); ?>:</strong>
			<span class="text"><?php echo wp_kses_post( get_user_meta( $vendor_user_id, '_company_phone', true ) ); ?></span>
		</li>
	</ul>
	<?php
}, 10, 4 );
